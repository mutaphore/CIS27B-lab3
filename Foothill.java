import java.util.Scanner;

public class Foothill {

	public static void main(String[] args) {

		 int rule;
	      String str_user_input;
	      Scanner input_stream = new Scanner(System.in);
	      Automaton aut;

	      // get rule from user
	      do
	      {
	         System.out.print("Enter Rule (0 - 255): ");
	         // get the answer in the form of a string:
	         str_user_input = input_stream.nextLine();
	         // and convert it to a number so we can compute:
	         rule = Integer.parseInt(str_user_input);

	      } while (rule < 0 || rule > 255);

	      // create automaton with this rule and single central dot
	      aut = new Automaton 
	         (rule, "                               *                               ");

	      // now show it
	      System.out.println("   start");
	      aut.ShowResults(32);
	      System.out.println("   end");
		
	}

}
