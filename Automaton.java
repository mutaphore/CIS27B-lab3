import java.math.*;

public class Automaton {

	 // class constants
   public final static int MAX_LEN  = 80;
   public final static int MAX_GENS  = 500;
   public final static String TRIPLET0 = "   ";
   public final static String TRIPLET1 = "  *";
   public final static String TRIPLET2 = " * ";
   public final static String TRIPLET3 = " **";
   public final static String TRIPLET4 = "*  ";
   public final static String TRIPLET5 = "* *";
   public final static String TRIPLET6 = "** ";
   public final static String TRIPLET7 = "***";
   
   // private members
   private boolean rules[];  // allocate rules[8] in constructor
   private StringBuffer this_gen; // same here
   
   // constructor
   public Automaton(int new_rule, String first_gen)
   {
      rules = new boolean[8];
      SetRule(new_rule);
      SetFirstGen(first_gen);
   }
  
   // mutators
   public boolean SetFirstGen(String first_gen)
   {
      StringBuffer sb_first_gen = new StringBuffer(first_gen);
      	
      // don't allow super-short OR super long automata
      if ( sb_first_gen.length() < 3 || sb_first_gen.length() > MAX_LEN)
      {
         this_gen = new StringBuffer(" *** ");
         return false;
      }

      // sanitize first_gen so all non-blank chars converted to '*'
      for (int k = 0; k < first_gen.length(); k++)
         if (sb_first_gen.charAt(k) !=  ' ')
            sb_first_gen.setCharAt(k, '*');

      this_gen = new StringBuffer(" ");
      this_gen.append(sb_first_gen);
      this_gen.append(" ");
      return true;
   }
   
   void SetRule(int new_rule)
   {
      
   	double remainder = new_rule;
   	
      // nothing to filter
      for (int k = 7; k >= 0; k--)
      {
      	if (remainder - Math.pow(2,k) >= 0) {
      		rules[k] = true;
      		remainder = remainder - Math.pow(2,k);
      	}
      	else	
      		rules[k] = false;
      }
        
   }
   
   public void ShowResults(int num_generations)
   {
      if (num_generations < 1 || num_generations > MAX_GENS )
         num_generations = MAX_GENS;
      for (int k = 0; k < num_generations; k++)
      {
         System.out.println(this_gen);
         PropagateNewGeneration();
      }
   }

   private void PropagateNewGeneration()
   {
      StringBuffer next_gen;
      
      // add leading position to far left
      next_gen = new StringBuffer(" ");
   
      // Propagate next generation based on rules[]
      for (int i = 0; i < this_gen.length()-2; i++) {
      	
      	if(rules[CheckTripletNum(i)] == true)	
      		next_gen.append("*");
      	else	
      		next_gen.append(" ");
      	
      }
      
      // add extra position to far right
      next_gen.append(" ");
   
      // and finally pass the torch to the new generation
      this_gen = next_gen;
   }
   
   //Returns an int representing the rule number that should be applied to
   //the triplet at the current index 
   private int CheckTripletNum(int index) {
   	
   	StringBuffer tempBuffer = new StringBuffer();
   	int returnValue;
   	
   	//Creates a temp buffer with the current triplet in this_gen
   	tempBuffer.append(this_gen.charAt(index));
   	tempBuffer.append(this_gen.charAt(index+1));
   	tempBuffer.append(this_gen.charAt(index+2));
   	
   	//Compare to predefined triplets and return the corresponding value
   	if (tempBuffer.toString().equals(TRIPLET0))	
   		returnValue = 0;
   	else if (tempBuffer.toString().equals(TRIPLET1))	
   		returnValue = 1;
   	else if (tempBuffer.toString().equals(TRIPLET2))	
   		returnValue = 2;
   	else if (tempBuffer.toString().equals(TRIPLET3))	
   		returnValue = 3;
   	else if (tempBuffer.toString().equals(TRIPLET4))	
   		returnValue = 4;
   	else if (tempBuffer.toString().equals(TRIPLET5))	
   		returnValue = 5;
   	else if (tempBuffer.toString().equals(TRIPLET6))	
   		returnValue = 6;
   	else
   		returnValue = 7;
   	
   	return returnValue;
   	
   }

	
}
